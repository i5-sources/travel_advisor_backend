
const Place = require("../models/Place");
const Saved = require("../models/Saved");
const decoded = require("../services/decodeToken");

const create = async(req, res) => {
    const { placeID } = req.body;
    try {
        const token = req.headers.authorization;
        const user = decoded(token);

        let saved = true;

        let findSaveUser = await Saved.find({
            place: placeID,
            user: user.data.user_id,
        });
        let saveUser = "";

        if (findSaveUser == "") {
            console.log(findSaveUser);
            saveUser = await Saved.create({
                place: placeID,
                saved: saved,
                user: user.data.user_id,
            });
            await saveUser.save();
            console.log(saveUser);

            //get information of place
            const place_doc = await Place.findById(placeID);

            //append new comment to Place
            place_doc.saved.push(saveUser._id);
            await place_doc.save();
        } else {
            findSaveUser[0].saved = !findSaveUser[0].saved;
            console.log(findSaveUser[0].saved);
            await findSaveUser[0].save();
        }
        res.status(200).json({ success: true, data: findSaveUser });
    } catch (error) {
        res.status(400).json({ success: false, error: error });
    }
};
const get_by_user = async(req, res) => {
    try {
        const token = req.headers.authorization;
        let user = "";
        if (!token) {
            user = {
                data: {
                    user_id: "62a9e1cc9585df0b1dbbec21",
                },
            };
        } else user = decoded(token);
        const get_saved = await Saved.find({
            user: user.data.user_id,
            saved: true,
        }).populate({
            path: "place",
            select: { title: 1, thumbnail: 1, story: 1, averageRating: 1 },
        });
        res.status(200).json({ success: true, data: get_saved });
    } catch (error) {
        res.status(400).json({ success: false, error: error });
    }
};
const get_by_place = async(req, res) => {
    const { id } = req.params;
    try {
        const token = req.headers.authorization;
        let user = "";
        if (!token) {
            user = {
                data: {
                    user_id: "62a9e1cc9585df0b1dbbec21",
                },
            };
        } else user = decoded(token);
        const get_saved = await Saved.find({
            user: user.data.user_id,
            saved: true,
            place: id,
        }).populate({
            path: "place",
            select: { title: 1, thumbnail: 1, story: 1, averageRating: 1 },
        });
        res.status(200).json({ success: true, data: get_saved });
    } catch (error) {
        res.status(400).json({ success: false, error: error });
    }
};

module.exports = {
    create,
    get_by_place,
    get_by_user,
};